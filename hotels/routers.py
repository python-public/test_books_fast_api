from datetime import datetime, date
from typing import List

from fastapi import APIRouter, Query
from fastapi_cache.decorator import cache
from pydantic import parse_obj_as

from .dao import HotelDAO
from .schemas import HotelInfo

hotels = APIRouter()


@hotels.get('/{location}')
@cache(expire=20)
async def get_hotels_by_location_and_time(
        location: str,
        date_from: date = Query(..., description=f'Например, {datetime.now().date()}'),
        date_to: date = Query(..., description=f'Например, {datetime.now().date()}'),
):
    """Контроллер сейчас не работает, нужно править запрос в HotelDAO.search_for_hotels"""
    hotels = await HotelDAO.search_for_hotels(location, date_from, date_to)
    hotels_json = parse_obj_as(List[HotelInfo], hotels)
    return hotels_json
