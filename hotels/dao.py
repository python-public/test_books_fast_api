from datetime import date

from sqlalchemy import select, func, and_, or_
from bookings.models import Bookings, Rooms
from dao.base import BaseDAO
from database import async_session_maker
from .models import Hotels


class HotelDAO(BaseDAO):
    model = Hotels

    @classmethod
    async def search_for_hotels(cls, location: str, date_from: date, date_to: date):
        # SELECT hotel_id, hotels.rooms_quantity - count(anon_2.room_id) as rooms_left, hotels.rooms_quantity
        # LEFT OUTER JOIN rooms ON rooms.hotel_id = hotels.id
        # LEFT OUTER JOIN (
        #     SELECT * FROM bookings
        #     WHERE (date_from < '2023-02-15' AND date_to > '2023-02-15')
        #     OR (date_from >= '2023-02-15' AND date_from < '2023-03-17')
        # ) AS anon_2
        # ON anon_2.room_id = rooms.id
        # WHERE (hotels.location LIKE 'Алтай')
        # GROUP BY hotels_id, hotels.rooms_quantity

        async with async_session_maker() as session:
            bookings_for_selected_dates = (
                select(Bookings).filter(
                    or_(
                        and_(Bookings.date_from < date_from, Bookings.date_to > date_from),
                        and_(Bookings.date_from >= date_from, Bookings.date_from < date_to),
                    )
                )
            ).subquery('filtered_bookings')

            hotels_rooms_left = (
                select(
                    (Hotels.rooms_quantity - func.count(bookings_for_selected_dates.c.room_id)).label('rooms_left'),
                Rooms.hotel_id,
                )
                .select_from(Hotels)
                .outerjoin(
                    bookings_for_selected_dates,
                    bookings_for_selected_dates.c.room_id == Rooms.id,
                )
                .where(Hotels.location.contains(location.title()),)
                .group_by(Hotels.rooms_quantity, Rooms.hotel_id)
                .cte('hotels_rooms_left')
            )

            get_hotels_info = (
                select(
                    Hotels.__table__.columns,
                    hotels_rooms_left.c.rooms_left,
                )
                .select_from(Hotels)
                .join(hotels_rooms_left, hotels_rooms_left.c.hotel_id == Hotels.id)
                .where(hotels_rooms_left.c.rooms_left > 0)
            )

            hotels_info = await session.execute(get_hotels_info)
            return hotels_info.all()

