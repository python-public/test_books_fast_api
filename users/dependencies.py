from datetime import datetime

from fastapi import Request, Depends
from jose import jwt, JWTError

from env import JWT
from .dao import UsersDAO
from .models import User
from exceptions import TokenAbsentException, TokenExpiredException, IncorrectTokenFormatException, \
    UserIsNotPresentException


def get_token(request: Request):
    token = request.cookies.get('booking_access_token')
    if not token:
        raise TokenAbsentException
    return token


async def get_current_user(token: str = Depends(get_token)) -> User:
    try:
        payload = jwt.decode(token, JWT.SECRET_KEY, JWT.ALGORITHM)
    except JWTError:
        raise IncorrectTokenFormatException
    expire: str = payload.get('exp')
    if (not expire) or (int(expire) < datetime.utcnow().timestamp()):
        raise TokenExpiredException
    user_id: str = payload.get('sub')
    if not user_id:
        raise UserIsNotPresentException
    user = await UsersDAO.find_by_id(int(user_id))
    if not user:
        raise UserIsNotPresentException
    return user
