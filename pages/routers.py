from fastapi import APIRouter, Request, Depends
from fastapi.templating import Jinja2Templates

pages = APIRouter(tags=['Фронтенд'])

templates = Jinja2Templates(directory='templates')


@pages.get('/hotels')
async def get_hotels_page(request: Request, hotels=Depends()):
    return templates.TemplateResponse(name='hotels.html', context={'request': request})
