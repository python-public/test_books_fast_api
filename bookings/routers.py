from datetime import date
from fastapi import APIRouter, Depends

from exceptions import RoomCannotBeBooked
from users.dependencies import get_current_user
from users.models import User
from .dao import BookingDAO
from .schemas import SBooking

bookings = APIRouter(tags=['Бронирования'])


@bookings.get('')
async def get_bookings(user: User = Depends(get_current_user)) -> list[SBooking]:
    print(user, type(user))
    return await BookingDAO.find_all(user_id=user.id)


@bookings.post('')
async def add_booking(room_id: int, date_from: date, date_to: date, user: User = Depends(get_current_user)):
    booking = await BookingDAO.add(room_id, date_from, date_to, user.id)
    if not booking:
        raise RoomCannotBeBooked
