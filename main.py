from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from redis import asyncio as aioredis

from hotels.routers import hotels
from bookings.routers import bookings
from pages.routers import pages
from users.routers import users

app = FastAPI()

app.include_router(router=users, prefix='/auth')
app.include_router(router=hotels, prefix='/hotels')
app.include_router(router=bookings, prefix='/bookings')

app.include_router(router=pages, prefix='/pages')

origins = [
    "http://localhost:3000",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
def startup():
    redis = aioredis.from_url("redis://localhost:6379", encoding="utf8", decode_responses=True)
    FastAPICache.init(RedisBackend(redis), prefix="cache")
