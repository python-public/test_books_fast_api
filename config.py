from fastapi import FastAPI
from env import DBConnect as EnvConfig


class DBConnect:
    __instance = None
    __DB_HOST: str
    __DB_PORT: int
    __DB_USER: str
    __DB_PASS: str
    __DB_NAME: str

    def __call__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instances = super().__call__(*args, **kwargs)
        return cls.__instance

    def __init__(self, host: str, name: str, user: str, password: str, port: int):
        self.__DB_HOST = host
        self.__DB_NAME = name
        self.__DB_USER = user
        self.__DB_PASS = password
        self.__DB_PORT = port

    def __del__(self):
        DBConnect.__instance = None

    def get_database_url(self):
        return \
            f'postgresql+asyncpg://{self.__DB_USER}:{self.__DB_PASS}@{self.__DB_HOST}:{self.__DB_PORT}/{self.__DB_NAME}'


db_con = DBConnect(EnvConfig.DB_HOST, EnvConfig.DB_NAME, EnvConfig.DB_USER, EnvConfig.DB_PASS, EnvConfig.DB_PORT)
app = FastAPI()
