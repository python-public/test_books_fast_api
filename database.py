from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from config import db_con


DATABASE_URL = db_con.get_database_url()
# создание «движка» SQLAlchemy
engine = create_async_engine(DATABASE_URL)
# создание сеанса бд
async_session_maker = async_sessionmaker(bind=engine, expire_on_commit=False, autoflush=False)
# класс от которого наследуются модели
Base = declarative_base()
